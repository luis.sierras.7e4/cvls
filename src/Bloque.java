//versió 0.2


public class Bloque {
    private final int terreno; // 0(tierra, verde) 1(roca, gris) 2(agua, azul)
    private Animal animal;


    public Bloque(int terreno, Animal animal) {
        this.animal = animal;
        this.terreno = terreno;
    }

    /**
     * @return una casilla según el terreno indicado
     */
    public String toString() {
        String grafico;
        switch (terreno) {
            case 1:
                grafico = "\033[0;100m";
                break;
            case 2:
                grafico = "\u001B[44m";
                break;
            default:
                grafico = "\u001B[42m";
        }
        if (animal == null) return grafico + "  " + "\033[0m";

        return grafico + animal + "\033[0m";
    }


    /**
     * @return true si se da el caso:
     */

    // comprobamos si es roca
    public boolean esRoca() {
        return terreno == 1;
    }

    // comprobamos si es agua
    public boolean esAgua() {
        return terreno == 2;
    }

    // comprobamos si es animal
    public boolean esAnimal() {
        return animal != null;
    }

    // comprobamos si el animal es de la subclase lobo
    public boolean esLobo() {
        return animal instanceof Lobo;
    }

    // comprobamos si el animal es de la subclase conejo
    public boolean esConejo() {
        return animal instanceof Conejo;
    }


    /**
     * Eliminamos animal del tablero
     */
    public void delAnimal() {
        this.animal = null;
    }

    /**
     * @return Animal que hay en una casilla
     */
    public Animal getAnimal() {
        return this.animal;
    }

    /**
     * Coloca el animal en la nueva posición
     *
     * @param animal es el nuevo animal
     */
    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
}